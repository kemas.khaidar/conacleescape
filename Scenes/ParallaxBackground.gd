extends ParallaxBackground

var offsetBg = 0

func _ready():
	set_process(true)
	get_node('../ThemeSound').play(0.0)
	
func _process(delta):
	offsetBg -= 300 * delta
	set_scroll_offset(Vector2(offsetBg, 0))