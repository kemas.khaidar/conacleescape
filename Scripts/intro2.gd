extends TextureButton

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var timer = null
var state = 0
func _ready():
	var timer = null
	timer = Timer.new()
	timer.connect("timeout", self, "on_timeout_complete")
	timer.set_wait_time(1)
	add_child(timer)
	timer.start()

func on_timeout_complete():
	get_tree().paused = false
	state = 0
	get_tree().change_scene("res://Scenes/intro3.tscn")
	# Called when the node is added to the scene for the first time.
	# Initialization here


#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
