extends TextureButton


func _ready():
	connect("pressed", self, "_on_pressed")
	pass

func _on_pressed():
	var data = {
    	coin = 0
	}

	var file = File.new()
	if file.open("user://.saved_game.sav", File.WRITE) != 0:
    	print("Error opening file")
    	return

	file.store_string(to_json(data))
	file.close()
	
	var data_highscore = {
    	high = 0
	}
	var file_highscore = File.new()
	if file_highscore.open("user://.saved_game2.sav", File.WRITE) != 0:
    	print("Error opening file")
    	return
	file_highscore.store_string(to_json(data_highscore))
	file_highscore.close()
	
	get_node('../../Ground/Total').set_text(str('TOTAL COIN: 0'))
	get_node('../../Ground/Highscore').set_text(str('HIGHSCORE: 0'))