extends KinematicBody2D

var input_direction = 0
var direction = 0

var screensize

var speed_x = 0
var speed_y = 0
var velocity = Vector2()
var jump = false

var current_coin = 0
var coin = 0
var collision_info = null

var is_able_to_pass_obstacle = false setget set_pass_obstacle
var power_up1 = false setget set_power_up1
var timer_obstacle = null
var timer_pup1 = null

const MAX_SPEED = 600
const ACCELERATION = 1000
const DECELERATION = 2000

const JUMP_FORCE = 1000
const GRAVITY = 2000

onready var sprite = get_node("AnimatedSpriteKemas")
onready var obstacle = preload("res://Scenes/Obstacles.tscn")

var anim = "run"
var is_roll = false

# bagian score dan highscore
var score = 0
var highscore = 0
var paused = false
var gameOver = false
############################


func _ready():
	set_process(true)
	set_process_input(true)
	screensize = get_viewport_rect (). size
	var file = File.new()
	timer_obstacle = Timer.new()
	timer_obstacle.set_one_shot(true)
	timer_obstacle.set_wait_time(5)
	timer_obstacle.connect("timeout", self, "on_timeout_complete")
	add_child(timer_obstacle)
	timer_pup1 = Timer.new()
	timer_pup1.set_one_shot(true)
	timer_pup1.set_wait_time(1)
	timer_pup1.connect("timeout", self, "on_timeout_complete1")
	add_child(timer_pup1)
	if not file.file_exists("user://.saved_game.sav"):
    	print("No file saved!")
    	return
	if file.open("user://.saved_game.sav", File.READ) != 0:
    	print("Error opening file")
    	return
	var data = {}
	data = parse_json(file.get_line())
	file.close()
	coin = data['coin']	
	get_node('../Ground/Total').set_text(str('TOTAL COIN: ', coin))
	get_node('../Ground/Coin').set_text(str('COIN: ', current_coin))
	# bagian highscore dan score
	var file1 = File.new()
	if not file1.file_exists("user://.saved_game2.sav"):
    	print("No file saved!")
    	return
	if file1.open("user://.saved_game2.sav", File.READ) != 0:
    	print("Error opening file")
    	return
	var data1 = {}
	data1 = parse_json(file1.get_line())
	file1.close()
	highscore = data1['high']
	get_node('../Ground/Score').set_text(str('SCORE: ', score))
	get_node('../Ground/Highscore').set_text(str('HIGHSCORE: ', highscore))
	##########################################


func _input(event):
	if event.is_action_pressed("ui_up"):
		if !jump:
			speed_y = -JUMP_FORCE
			jump=true
			if is_able_to_pass_obstacle:
				anim = "jumpPowerup"
				sprite.play(anim)
			else:
				anim = "jump"
				sprite.play(anim)
			get_node('../JumpSound').play(0.0)
	elif event.is_action_pressed("ui_down"):
		is_roll = true
		get_node('../SlideSound').play(0.0)
	pass


func _process(delta):
	# INPUT
	if input_direction:
		direction = input_direction
	
	#if Input.is_action_pressed("ui_left"):
	#	input_direction = -1
	#elif Input.is_action_pressed("ui_right"):
	#	input_direction = 1
	#else:
	#	input_direction = 0
	
	# MOVEMENT
	if input_direction == - direction:
		speed_x /= 3
	if input_direction:
		speed_x += ACCELERATION * delta
	else:
		speed_x -= DECELERATION * delta
	speed_x = clamp(speed_x, 0, screensize.x)
	
	speed_y += GRAVITY * delta
	
	if(power_up1):
		velocity.x = 10
		if(jump):
			velocity.x = 2
	else:
		velocity.x = speed_x * delta * direction
	velocity.y = speed_y * delta
	var collision_info = move_and_collide(velocity)
	#if(collision_info):
	#	print(collision_info.collider.get_property_list())
	if collision_info:
		jump = false
		speed_y = 0
		if is_able_to_pass_obstacle && is_roll:
			anim = "slidePowerUp"
			sprite.time = 0.01
		elif is_able_to_pass_obstacle:
			anim = "runPowerup"
		elif is_roll:
			anim = "slide"
			sprite.time = 0.01
		else:
			anim = "run"
		sprite.play(anim)
	#highscore and score
	add_score(delta)
	eval_score()
		
func add_coin(delta):
	current_coin += 1
	coin += 1
	var data = {
    	coin = coin
	}

	var file = File.new()
	if file.open("user://.saved_game.sav", File.WRITE) != 0:
    	print("Error opening file")
    	return

	file.store_string(to_json(data))
	file.close()
	get_node('../Ground/Total').set_text(str('TOTAL COIN: ', coin))
	get_node('../Ground/Coin').set_text(str('COIN: ', current_coin))

func play_coin():
	get_node('../CoinSound').play(0.0)
	
func pup_sound():
	get_node('../PupSound').play(0.0)

func add_score(delta):
	score += delta
	get_node('../Ground/Score').set_text(str('SCORE: ', round(score)))
	
func eval_score():
	if round(score) > highscore:
		highscore = round(score)
		var data_highscore = {
    		high = highscore
		}
		print(to_json(data_highscore))
		var file_highscore = File.new()
		if file_highscore.open("user://.saved_game2.sav", File.WRITE) != 0:
    		print("Error opening file")
    		return
		file_highscore.store_string(to_json(data_highscore))
		file_highscore.close()
	
		get_node('../Ground/Highscore').set_text(str('HIGHSCORE: ', highscore))
		get_node('../Ground/Score').set_text(str('SCORE: ', highscore))
		
func set_pass_obstacle(new_value):
	is_able_to_pass_obstacle = new_value
	if is_able_to_pass_obstacle:
		print("coba power up")
		if jump:
			anim = "jumpPowerup"
		else:
			anim = "runPowerup"
		sprite.play(anim)
		timer_obstacle.start()
		#is_able_to_pass_obstacle = false
	pass
	
func set_power_up1(new_value):
	power_up1 = new_value
	if power_up1:
		#speed_x += ACCELERATION
		print(velocity)
		#velocity.y = speed_y * delta
		#var collision_info = move_and_collide(velocity)
		print("powerup1 get")
		timer_pup1.start()
	
	
func on_timeout_complete():
	print("coba power up1")
	if jump:
		anim = "jump"
	else:
		anim = "run"
	sprite.play(anim)
	is_able_to_pass_obstacle = false
	
func on_timeout_complete1():
	print("powerup1 done")
	power_up1 = false

func save_run():
	var data = {
    	coin = current_coin,
		score = round(score)
	}

	var file = File.new()
	if file.open("user://.last_run.sav", File.WRITE) != 0:
    	print("Error opening file")
    	return

	file.store_string(to_json(data))
	file.close()