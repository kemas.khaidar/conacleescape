extends CollisionShape2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var trans = get_transform()
var old = trans.get_scale()

func _ready():
	set_process(true)
   
func _process(delta):
	if(get_parent().is_roll):
		position=(Vector2(24.404959,23.33828))
		set_scale(Vector2(2.369119, 0.564731))
	else:		
		position=(Vector2(-1.78231,-2.78489))
		set_scale(old)