extends Node2D

# bagian score dan highscore
var score = 0
var highscore = 0
var paused = false
var gameOver = false
############################
var current_coin = 0
var coin = 0

func _ready():
	var file = File.new()
	if not file.file_exists("user://.saved_game.sav"):
    	print("No file saved!")
    	return
	if file.open("user://.saved_game.sav", File.READ) != 0:
    	print("Error opening file")
    	return
	var data = {}
	data = parse_json(file.get_line())
	file.close()
	coin = data['coin']	
	get_node('../Ground/Total').set_text(str('TOTAL COIN: ', coin))
	
	var file1 = File.new()
	if not file1.file_exists("user://.saved_game2.sav"):
    	print("No file saved!")
    	return
	if file1.open("user://.saved_game2.sav", File.READ) != 0:
    	print("Error opening file")
    	return
	var data1 = {}
	data1 = parse_json(file1.get_line())
	file1.close()
	highscore = data1['high']
	get_node('../Ground/Highscore').set_text(str('HIGHSCORE: ', highscore))
	
	var file2 = File.new()
	if not file2.file_exists("user://.last_run.sav"):
    	print("No file saved!")
    	return
	if file2.open("user://.last_run.sav", File.READ) != 0:
    	print("Error opening file")
    	return
	var data3 = {}
	data3 = parse_json(file2.get_line())
	current_coin = data3['coin']
	score = data3['score']
	get_node('../Ground/Coin').set_text(str('COIN COLLECTED: ', current_coin))
	get_node('../Ground/Score').set_text(str('YOUR SCORE: ', score))
	file2.close()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
