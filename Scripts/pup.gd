extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var speed = 300

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	position.x -= speed * delta
	if position.x <-50:
		queue_free()
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_pup_body_entered(body):
	#power up
	body.is_able_to_pass_obstacle = true
	body.pup_sound()
	if (body.name == "Kemas"):
		queue_free()

