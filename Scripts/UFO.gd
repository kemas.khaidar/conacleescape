extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var speed = 0
var screensize

func _ready():
	screensize = get_viewport_rect().size
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	#position.x += speed * delta
	if position.x > screensize.x+50:
		queue_free()
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_UFO_body_entered(body):
	if(body.get_name()=="Kemas"):
		body.eval_score()
		body.save_run()
		if(body.coin < 100):
			body.queue_free()
			get_tree().change_scene("res://Scenes/menu_title.tscn")
		else:
			get_node("../Pause").secondChance()