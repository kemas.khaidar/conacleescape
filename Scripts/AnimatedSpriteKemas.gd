extends AnimatedSprite

var tempElapsed = 0
var time = 0.1
var counter = 0

func _ready():
	set_process(true)
   
func _process(delta):
	if(get_parent().anim=="slide"):
		offset = Vector2(0, 230)
		scale = Vector2(0.06704, 0.06704)
		time = 0.01
		tempElapsed = tempElapsed + delta
		counter += 1
		
		if(tempElapsed > time):
			if(get_frame() == self.get_sprite_frames().get_frame_count('default')-1):
	 			set_frame(0)
			else:
				self.set_frame(get_frame() + 1)
	      
			tempElapsed = 0
			
		if(counter == 50):
			get_parent().is_roll=false
			offset = Vector2(0,0)
			scale = Vector2(0.05704, 0.05704)
			counter = 0
	
	if(get_parent().anim=="slidePowerUp"):
		offset = Vector2(0, 230)
		scale = Vector2(0.06704, 0.06704)
		time = 0.01
		tempElapsed = tempElapsed + delta
		counter += 1
		
		if(tempElapsed > time):
			if(get_frame() == self.get_sprite_frames().get_frame_count('default')-1):
	 			set_frame(0)
			else:
				self.set_frame(get_frame() + 1)
	      
			tempElapsed = 0
			
		if(counter == 50):
			get_parent().is_roll=false
			offset = Vector2(0,0)
			scale = Vector2(0.05704, 0.05704)
			counter = 0