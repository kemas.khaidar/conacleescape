extends KinematicBody2D

var input_direction = 0
var direction = 0

var speed_x = 0
var speed_y = 0
var velocity = Vector2()
var jump = false

var current_coin = 0
var coin = 0
var collision_info = null

var is_able_to_pass_obstacle = false setget set_pass_obstacle
var timer_obstacle = null

const MAX_SPEED = 600
const ACCELERATION = 1000
const DECELERATION = 2000

const JUMP_FORCE = 1000
const GRAVITY = 2000

onready var sprite = get_node("AnimatedSprite")

var anim = "run"

# bagian score dan highscore
var score = 0
var highscore = 0
var paused = false
var gameOver = false
############################


func _ready():
	set_process(true)
	set_process_input(true)
	timer_obstacle = Timer.new()
	timer_obstacle.set_one_shot(true)
	timer_obstacle.set_wait_time(5)
	timer_obstacle.connect("timeout", self, "on_timeout_complete")
	add_child(timer_obstacle)
	var data = {
    	coin = 0,
		score = 0
	}

	var file = File.new()
	if file.open("res://.last_run.sav", File.WRITE) != 0:
    	print("Error opening file")
    	return

	file.store_string(to_json(data))
	file.close()


func _input(event):
	if event.is_action_pressed("ui_up"):
		if !jump:
			speed_y = -JUMP_FORCE
			jump=true
			if is_able_to_pass_obstacle:
				anim = "jumpPowerup"
				sprite.play(anim)
			else:
				anim = "jump"
				sprite.play(anim)
	pass


func _process(delta):
	# INPUT
	if input_direction:
		direction = input_direction
	
	#if Input.is_action_pressed("ui_left"):
	#	input_direction = -1
	#elif Input.is_action_pressed("ui_right"):
	#	input_direction = 1
	#else:
	#	input_direction = 0
	
	# MOVEMENT
	if input_direction == - direction:
		speed_x /= 3
	if input_direction:
		speed_x += ACCELERATION * delta
	else:
		speed_x -= DECELERATION * delta
	speed_x = clamp(speed_x, 0, MAX_SPEED)
	
	speed_y += GRAVITY * delta

	velocity.x = speed_x * delta * direction
	velocity.y = speed_y * delta
	var collision_info = move_and_collide(velocity)
	if collision_info:
		jump = false
		speed_y = 0
		if is_able_to_pass_obstacle:
			anim = "runPowerup"
		else:
			anim = "run"
		sprite.play(anim)
	
func set_pass_obstacle(new_value):
	is_able_to_pass_obstacle = new_value
	if is_able_to_pass_obstacle:
		print("coba power up")
		if jump:
			anim = "jumpPowerup"
		else:
			anim = "runPowerup"
		sprite.play(anim)
		timer_obstacle.start()
		#is_able_to_pass_obstacle = false
	pass
	
func on_timeout_complete():
	print("coba power up1")
	if jump:
		anim = "jump"
	else:
		anim = "run"
	sprite.play(anim)
	is_able_to_pass_obstacle = false
