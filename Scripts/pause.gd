extends TextureButton

var state = 0
var timer = null
var button = null
var coin = 0

func _ready():
	var buttonMenu = get_node("../menu")
	buttonMenu.connect("pressed", self, "on_menu_pressed")
	pass

func on_menu_pressed():
	get_tree().paused = false
	state = 0
	get_tree().change_scene("res://Scenes/main_title.tscn")
	pass
	
func _on_Pause_pressed():
	if state == 0:
		get_tree().paused = true
		state = 1
		get_node("../Paused").show()
		get_node("../menu").show()
	elif state == 1:
		get_tree().paused = false
		state = 0
		get_node("../Paused").hide()
		get_node("../menu").hide()
	pass # replace with function body

func _input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ESCAPE:
			if state == 0:
				get_tree().paused = true
				state = 1
				get_node("../Paused").show()
				get_node("../menu").show()
			elif state == 1:
				get_tree().paused = false
				state = 0
				get_node("../Paused").hide()
				get_node("../menu").hide()
				
func secondChance():
	get_tree().paused = true
	state = 1
	get_node("../SecondChance").show()
	button = get_node("../SecondChance")
	button.connect("pressed", self, "on_Button_pressed")
	timer = Timer.new()
	timer.connect("timeout", self, "on_timeout_complete")
	timer.set_wait_time(3)
	add_child(timer)
	timer.start()
		
func on_timeout_complete():
	get_tree().paused = false
	state = 0
	get_tree().change_scene("res://Scenes/menu_title.tscn")

func on_Button_pressed():
	timer.stop()
	
	var file = File.new()
	if not file.file_exists("user://.saved_game.sav"):
    	print("No file saved!")
    	return
	if file.open("user://.saved_game.sav", File.READ) != 0:
    	print("Error opening file")
    	return
	var data = {}
	data = parse_json(file.get_line())
	file.close()
	coin = data['coin']
	
	coin = coin-100
	data = {
    	coin = coin
	}

	file = File.new()
	if file.open("user://.saved_game.sav", File.WRITE) != 0:
    	print("Error opening file")
    	return

	file.store_string(to_json(data))
	file.close()
	
	var kemas = get_node("../Kemas")
	kemas.coin = coin
	kemas.position.x = 256
	kemas.position.y = 0
	
	get_node('../Ground/Total').set_text(str('TOTAL COIN: ', coin))
	
	get_tree().paused = false
	state = 0
	get_node("../SecondChance").hide()
