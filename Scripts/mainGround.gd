extends Node2D

var highscore = 0
var coin = 0

func _ready():
	#var dir = Directory.new()
	#dir.copy("res://.saved_game.sav", "user://.saved_game.sav")
	#dir.copy("res://.saved_game2.sav", "user://.saved_game2.sav")
	var file = File.new()
	if not file.file_exists("user://.saved_game.sav"):
    	print("No file saved!")
    	return
	if file.open("user://.saved_game.sav", File.READ) != 0:
    	print("Error opening file")
    	return
	var data = {}
	data = parse_json(file.get_line())
	file.close()
	coin = data['coin']	
	get_node('../Ground/Total').set_text(str('TOTAL COIN: ', coin))
	
	var file1 = File.new()
	if not file1.file_exists("user://.saved_game2.sav"):
    	print("No file saved!")
    	return
	if file1.open("user://.saved_game2.sav", File.READ) != 0:
    	print("Error opening file")
    	return
	var data1 = {}
	data1 = parse_json(file1.get_line())
	file1.close()
	highscore = data1['high']
	get_node('../Ground/Highscore').set_text(str('HIGHSCORE: ', highscore))
	get_node('../ThemeSound').play(0.0)
	
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
