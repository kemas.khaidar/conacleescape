extends Node2D

var screensize
export var speed = 200
var timer = null
var timerPowerUp = null
var timerPowerUp1 = null
var obstacle_delay = 2.0
var can_jump = true
var show_pup = true
var show_pup1 = true
var state = 0
var counter = 0
var obs_speed = 400

onready var obstacle = preload("res://Scenes/Obstacles.tscn")
onready var coin = preload("res://Scenes/Koin.tscn")
onready var pup = preload("res://Scenes/pup.tscn")
onready var pup1 = preload("res://Scenes/pup1.tscn")

func _ready():
	screensize = get_viewport_rect().size
	timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(obstacle_delay)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)
	
	timerPowerUp = Timer.new()
	timerPowerUp.set_one_shot(true)
	timerPowerUp.set_wait_time(50)
	timerPowerUp.connect("timeout", self, "on_timeout_complete_pup")
	add_child(timerPowerUp)
	
	
	timerPowerUp1 = Timer.new()
	timerPowerUp1.set_one_shot(true)
	timerPowerUp1.set_wait_time(30)
	timerPowerUp1.connect("timeout", self, "on_timeout_complete_pup1")
	add_child(timerPowerUp1)
	
	get_node("Paused").hide()
	get_node("menu").hide()
	get_node("SecondChance").hide()
	get_node('MainThemeSound').play(0.0)
	
func on_timeout_complete():
	can_jump = true

func on_timeout_complete_pup():
	show_pup = true
	var powerup = pup.instance()
	var tempObs = rand_range(7,10)
	powerup.position.x = screensize.x
	powerup.position.y = 50*tempObs
	add_child(powerup)
	
func on_timeout_complete_pup1():
	show_pup1 = true
	var powerup = pup1.instance()
	var tempObs = rand_range(7,10)
	powerup.position.x = screensize.x
	powerup.position.y = 50*tempObs
	add_child(powerup)

func _process(delta):
	if (can_jump):
		var n_obstacle = obstacle.instance()
		counter += 1
		if (counter %5 == 0 and obs_speed < 900):
			if (obstacle_delay > 0.15):
				obstacle_delay -= 0.15
			elif (obstacle_delay < 0.015 && obstacle_delay > 0.02):
				obstacle_delay -= 0.01
			timer.set_wait_time(obstacle_delay)
			obs_speed += 50
			print(obstacle_delay)
		n_obstacle.speed = obs_speed
		var temp = rand_range(8, 12)
		n_obstacle.position.x = screensize.x
		var tempa = 40*temp
		n_obstacle.position.y = tempa
		add_child(n_obstacle)
		var temp2 = randi()%10+1
		while (temp2 == temp):
			temp2 = randi()%10+1
		for i in range(randi()%10+1):
			var koin = coin.instance()
			koin.position.x = screensize.x + (i*20)
			koin.position.y = 50*(temp2)
			add_child(koin)
		can_jump=false
		timer.start()
	if (show_pup):
		timerPowerUp.start()
		show_pup = false
	if (show_pup1):
		timerPowerUp1.start()
		show_pup1 = false
	#print(get_node("Kemas").is_able_to_pass_obstacle)
	if(get_node("Kemas").is_able_to_pass_obstacle):
		#print("okeoke")
		if(get_node("Obstacles")):
			print("kaow")
			get_node("Obstacles/CollisionShape2D").disabled = true
	#print_tree_pretty()
